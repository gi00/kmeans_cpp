#ifndef KMEANS_H
#define KMEANS_H

#include <algorithm>
#include <cassert>
//#include <charconv>
#include <cmath>
#include "container.h"
#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>
#include <string>
#include <random>

namespace kmeans {

    namespace io { 

        template <typename T, const size_t N, const size_t D>
        mat<T, N, D> load_csv(const std::string& path, const char delim) {
            /* Loads the data contained in a .csv file.
             *
             * @param path String with the relative path to the data.
             * @param delim Delimitation character that separates the data row-wise.
             * @return data_matrix A NxD matrix containing the data points.
             */
            std::ifstream file(path);
            std::string line;
            mat<T, N, D> data_matrix;
            for (size_t i = 0; i < N; ++i) {
                std::getline(file, line);
                std::stringstream line_stream(line);
                std::string cell;
                vec<T, D> point;
                for (size_t j = 0; j < D; ++j) {
                    std::getline(line_stream, cell, delim);
                    //trovare il modo di usare from_chars
                    //T val;
                    //auto res = std::from_chars(cell.data(), cell.data() + cell.size(), val);
                    point[j] = static_cast<T>(std::stod(cell));
                }
                data_matrix[i] = point;
            }
            return data_matrix;
        }

        template <typename T, const size_t D>
        void print_vec(const vec<T, D>& vector) {
            for (auto v : vector)
                std::cout << v << ' ';
            std::cout << '\n';
            return;
        }

        template <typename T>
        void print_vec(const std::vector<T>& vector) {
            for (auto v : vector)
                std::cout << v << ' ';
            std::cout << '\n';
            return;
        }

        template <typename T, const size_t N, const size_t D>
        void print_mat(const mat<T, N, D>& matrix) {
            for (const auto& vector : matrix)
                print_vec<T, D>(vector);
            std::cout << '\n';
            return;
        }

    } // namespace io

    namespace calc {

        template<typename T, const size_t D>
        double calc_distance(const vec<T, D>& p, const vec<T, D>& q) {
            /* Calculates the squared L2 euclidean distance between two vectors.
             */
            double sum = 0.0;
            for (size_t i = 0; i < D; ++i)
                sum += ((p[i] - q[i]) * (p[i] - q[i]));
            return sum;
        }

        template<typename T, const size_t M, const size_t D>
        bool has_converged(const mat<T, M, D>& prev_cent, const mat<T, M, D>& curr_cent, const double eps) {
            vec<double, M> distances;
            for (size_t i = 0; i < M; ++i)
                distances[i] = calc_distance(prev_cent[i], curr_cent[i]);
            return std::all_of(distances.begin(), distances.end(), [eps](double d){return d <= eps;});
        }

        template<typename T, const size_t M, const size_t D>
        size_t get_closest_centroid(const vec<T, D>& point, const mat<T, M, D>& centroids) {
            /* Returns the index of the least distanced element in a matrix wrt a point.
             *
             * Given a data point p this method returns the closest (candidate) cluster center.
             *
             * @param point The 1xD point we want to associate to a (candidate) cluster center. 
             * @param centroids MxD A matrix of cluster centers.
             */
            vec<double, M> distances;
            for (size_t i = 0; i < M; ++i)
                distances[i] = calc_distance<T, D>(point, centroids[i]);
            auto min_dist = std::min_element(distances.begin(), distances.end());
            return std::distance(distances.begin(), min_dist);
        }

        template <typename T, const size_t N, const size_t M, const size_t D>
        vec<size_t, N> calc_assignment(const mat<T, N, D>& data, const mat<T, M, D>& centroids) {
            /* Computes the association (point, closest centroid) for every data point.
             *
             * In this case this association is represented by a vector of the same size as the
             * data set, where every entry is just the index of the closest (candidate) cluster center.
             * 
             * @param data The NxD data matrix of all points.
             * @param centroids MxD The matrix of (candidate) cluster centers.
             */
            vec<size_t, N> assignment;
            for (size_t i = 0; i < N; ++i)
                assignment[i] = get_closest_centroid<T, M, D>(data[i], centroids);
            return assignment;
        }

        template <typename T, const size_t N, const size_t M, const size_t D>
        mat<T, M, D> update_centroids(const mat<T, N, D>& data, const vec<size_t, N>& assignment) {
            /* Updates every candidate cluster center to the barycenter (mean) of the set of
             * points associated with it.
             * 
             * @param data The NxD data matrix of all points.
             * @param assignment The Nx1 vector of indices indicating which 
             * (candidate) cluster center is the closest. 
             * @return The MxD matrix of freshly updated centroids.
             */
            mat<T, M, D> new_centroids {};
            vec<size_t, M> counts {};
            for (size_t i = 0; i < N; ++i) {
                new_centroids[assignment[i]] = new_centroids[assignment[i]] + data[i];
                counts[assignment[i]] += 1;
            }
            for (size_t i = 0; i < M; ++i)
                new_centroids[i] = new_centroids[i] / counts[i];
            return new_centroids;
        }

        template <typename T, const size_t N, const size_t M, const size_t D>
        mat<T, M, D> kmeans(const mat<T, N, D>& data, mat<T, M, D>& centroids, const size_t niter) {
            /* Running the assignment step followed by the update step for a fixed number of iterations.
             *
             * @param data The NxD data matrix of all points.
             * @param centroids The MxD matrix of the initials cluster centers.
             * @param niter The total number of iterations.
             * @return centroids The MxD matrix of finals cluster centers.
             */
            for (size_t i = 0; i < niter; ++i) {
                vec<size_t, N> assignment = calc_assignment<T, N, M, D>(data, centroids);
                centroids = update_centroids<T, N, M, D>(data, assignment);
            }
            return centroids;
        }

        template <typename T, const size_t N, const size_t M, const size_t D>
        mat<T, M, D> kmeans(const mat<T, N, D>& data, mat<T, M, D>& centroids, const size_t niter, const double eps) {
            /* Running the assignment step followed by the update step for a fixed number of iterations.
             *
             * Centroids are returned when their euclidean distance wrt to the previous candidates
             * is lower then the threshold `eps`. If this condition never occurs the algorithm
             * goes on for `niter`.
             * 
             * @param data The NxD data matrix of all points.
             * @param centroids The MxD matrix of the initials cluster centers.
             * @param niter The total number of iterations.
             * @param eps Tolerance for establishing the convergence.
             * @return centroids The MxD matrix of finals cluster centers.
             */
            for (size_t i = 0; i < niter; ++i) {
                vec<size_t, N> assignment = calc_assignment<T, N, M, D>(data, centroids);
                mat<T, M, D> current_cent = update_centroids<T, N, M, D>(data, assignment);
                if (has_converged<T, M, D>(centroids, current_cent, eps)) {
                    std::cout << "Converged at " << i << "th iteration.\n";
                    return current_cent;
                }
                centroids = current_cent;
            }
            return centroids;
        }

    } // namespace calc

    namespace sample {

        template <typename T, const size_t N, const size_t M, const size_t D>
        // capire se si puo usare shuffle del c++17
        mat<T, M, D> sample_centroids_from_data(const mat<T, N, D>& data) {
            /* Random initialization of the centroids. Sampled (without replacement)
             * from the data set.
             * 
             * @param data The NxD data matrix of all points.
             * @return centroids The MxD matrix of randomly sampled centroids.
             */
            static std::random_device seed;
            static std::mt19937 random_number_generator(seed());
            std::vector<size_t> range(N);
            std::iota(range.begin(), range.end(), 0);
            std::shuffle(range.begin(), range.end(), random_number_generator);
            mat<T, M, D> centroids;
            for (size_t i = 0; i < M; ++i)
                centroids[i] = data[range[i]];
            return centroids;
        }

    } // namespace sample

} // namespace kmeans

#endif