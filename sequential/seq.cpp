#include <chrono>
#include "kmeans.h"

int main() {

    const size_t num_points = 100000;
    const size_t dim = 2;
    const size_t k = 3;
    const size_t niter = 500;
    const std::string data_path = "../datasets/2d/100000_samples_3_centers/data.csv";
    const double eps = 1e-4;

    const kmeans::mat<float, num_points, dim> data = kmeans::io::load_csv<float, num_points, dim>(data_path, ',');
    kmeans::mat<float, k, dim> sampled_centroids = kmeans::sample::sample_centroids_from_data<float, num_points, k, dim>(data);
    if (eps > 0) {
        auto start = std::chrono::high_resolution_clock::now();
        const kmeans::mat<float, k, dim> centroids = kmeans::calc::kmeans<float, num_points, k, dim>(data, sampled_centroids, niter, eps);    
        auto end = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        std::cout << "Sequential Duration: " << duration << " ms" << "\n\n";
        kmeans::io::print_mat(centroids);
    } else {
        auto start = std::chrono::high_resolution_clock::now();
        const kmeans::mat<float, k, dim> centroids = kmeans::calc::kmeans<float, num_points, k, dim>(data, sampled_centroids, niter);    
        auto end = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        std::cout << "Sequential Duration: " << duration << " ms" << "\n\n";
        kmeans::io::print_mat(centroids);
    }
    return 0;
}