# k-means clustering

![](https://i.imgur.com/S65Sk9c.jpg)

## Brief description

k-means is a simple and popular clustering technique. It is a standard baseline when the number of cluster centers (*k*) is known (or almost known) a-priori.

> Given a set of observations (*x1, x2, ..., xn*), where each observation is a *d*-dimensional real vector, *k*-means clustering aims to partition the *n* observations into *k <= n* sets so as to minimize the within-cluster sum of squares (i.e. variance) [[1]]. 

In its naive form (also known as the Lloyd's version) the algorithm is composed of two steps:

- **Assignment step**: Assign each observation to the cluster with the nearest mean: that is the one with the least squared Euclidean distance.

- **Update step**: Recalculate means (centroids) for observations assigned to each cluster. 

The average complexity is given by *O(knT)*, were *n* is the number of samples and *T* is the number of iteration.

Although being a computationally hard problem (NP-hard) this algorithm is, in practice, very fast, but it falls in local minima. Given these properties it is usually restarted several times, in order to get a better estimate of the real centroids.

## Code

k-means works for Euclidean spaces of arbitrary dimensionality (obviously suffering the [curse of dimensionality](https://en.wikipedia.org/wiki/Curse_of_dimensionality)) and in this implementation this number will be not fixed a-priori. 

A few simple synthetic datasets of various dimensionalities (2 and 3) and data size (500 to 100k data points) are placed under `.datasets/`. For example 

```
./datasets/2d/100000_samples_3_centers
```

is a directory containing a dataset of 100000 points in 2D synthetically generated around 3 cluster centers (centroids). Inside every directory there will be 2 `csv` files:

1. `centroids.csv`: The real centroids (that should match the ones computed by k-means).
2. `data.csv`: The actual data to be clustered.


### Sequential version

This module is structured as following:

- `container.h`: Header file containing the definitions of the data structures mainly used in this project. These are simply arrays and arrays of arrays (namely `vec` and `mat`). This header contains also a couple of useful operator overloads for these structures.
- `kmeans.h`: The main header file where the necessary methods are coded: these include a number of I/O methods (data reading, console printing, ...), a sampling method (for the random initialization of the centroids) and the core functions of the k-means (the assignment and update steps). These methods are grouped under common namespaces for better code management.
- `Makefile`: Very (very) simple makefile needed just for alternating between debug and release mode. 
- `seq.cpp`: Source file where the functionality is tested.

Since this implementation is centered on **performance** the user has to specify every time the "*hyperparameters*" of the algorithm, in order to help the compiler optimize the code. This is done in the `main` function inside `seq.cpp`, where these lines have to be edited each time

```cpp
const size_t num_points = 100000;
const size_t dim = 2;
const size_t k = 3;
const size_t niter = 500;
const std::string data_path = "../datasets/2d/100000_samples_3_centers/data.csv";
const double eps = 1e-4;
```

In particular

- `num_points` is the number of data points (**Warning**: this number needs to match the one preceding `_samples` in the `data_path` variable).
- `dim` is the dimensionality of the data set (**Warning**: this number needs to match the one indicating the sub-directory in the `data_path` variable).
- `k` is the number of cluster centers (**Warning**: this number needs to match the one preceding `_centers` in the `data_path` variable).
- `niter` is the number of iterations that the algorithm will run through.
- `data_path` is the string containing the path to the `data.csv` that the user wants to cluster.
- `eps` is the tolerance value for establishing the convergence of the algorithm. When all the candidate centroids have moved of a distance <= eps wrt the previous candidates, then the algorithm will stop. If set to a value less or equal to zero this feature will not be used and the algorithm will run for `niter`.

Once these are set up the user has to run

```
$ make
$ ./kmeans_seq
```
and the algorithm will execute. In this early version the centroids computed by k-means will be printed to the console. The main function (`kmeans::calc::kmeans`) will be timed and the elapsed time will be displayed as well.

For debugging run
```
$ make sequential_db
$ ./kmeans_seq
```

The compiler I use is [g++](https://man7.org/linux/man-pages/man1/g++.1.html) 10.2.0 with `-std=c++17`.

### Parallel version

---

[1]: https://en.wikipedia.org/wiki/K-means_clustering
